package com.sun.utils;

import java.util.List;

/**
 * 总页数是计算出来的，设置行数的时候进行赋值
 * @param <T>
 */
public class PageBean <T>{
    // 返回的list
    private List<T> list;

    // 每页多少条数据
    private Integer pageSize;

    // 当前第几页
    private Integer pageNo;

    // 总页数
    private Integer totalPage;

    // 总行数
    private Integer rows;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
        // 计算总页数
        this.totalPage = rows % pageSize == 0 ? rows / pageSize : (rows / pageSize) + 1;
    }

    public void setPageNo(Integer pageNo) {
        if (null == pageNo || pageNo < 0 ){
            this.pageNo = 1;
        }else if (pageNo > this.totalPage && this.totalPage > 0){
            this.pageNo = totalPage;
        }else {
            this.pageNo = pageNo;
        }
    }

    /**
     * @param pageSize 每页条数
     * @param pageNo 当前页
     * @param rows 总行数
     * @return 初始化后的PageBean
     */
   public PageBean<T> initiallze(Integer pageSize,Integer pageNo,int rows){
        this.setPageSize(pageSize);
        this.setRows(rows);
        this.setPageNo(pageNo);
        return  this;
   }
}
