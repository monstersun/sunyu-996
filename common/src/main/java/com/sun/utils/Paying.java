package com.sun.utils;

import lombok.Data;

/**
 * 封装查询对象
 */
@Data
public class Paying<T>{
    // 查询条件封装
    private Object object;

    private PageBean<T> pageBean;

    private Integer offset;

    private Integer limit;

    public Paying<T> initialize(){
        return null;
    }

}
