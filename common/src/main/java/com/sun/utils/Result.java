package com.sun.utils;

public class Result<T> {
    // 返回的信息
    private String msg;

    // 请求状态
    private Boolean state = true;

    // 返回值
    private T data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
