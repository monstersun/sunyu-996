package com.sun.mapper;

import com.sun.dto.UserDTO;
import com.sun.pojo.User;
import com.sun.utils.Paying;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    List<User> getUserListByPaying(Paying<User> paying);

    int getUserListRows(@Param("userDTO") UserDTO userDTO);


}
