package com.sun.pojo;

import com.sun.dto.UserDTO;
import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;
    private String username;
    private String password;
    private Integer age;
    private Date birthday;
    private String address;
    private Date creatTime;
    private Date updateTime;

    public UserDTO convert(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setAge(user.getAge());
        userDTO.setBirthday(user.getBirthday());
        userDTO.setAddress(user.getAddress());
        userDTO.setCreatTime(user.getCreatTime());
        userDTO.setUpdateTime(user.getUpdateTime());
        return userDTO;

    }

}
