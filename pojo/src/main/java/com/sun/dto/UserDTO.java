package com.sun.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {
    private Integer id;
    private String username;
    private String password;
    private Integer age;
    private Date birthday;
    private String address;
    private Date creatTime;
    private Date updateTime;

}
