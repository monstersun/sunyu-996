package com.sun.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * user
 * @author 
 */
@Data
public class User implements Serializable {
    /**
     * id
     */
    private Integer id;

    private String username;

    private String password;

    private Integer age;

    private Date birthday;

    /**
     * 地址
     */
    private String address;

    private Date creatTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;
}