package com.sun.service;

import com.sun.dto.UserDTO;
import com.sun.utils.Paying;

import java.util.List;

public interface UserService<T> {

    int getRowsByUserDTO(UserDTO userDTO);

    List<T> getUserListByPaying(Paying<T> paying);

}
