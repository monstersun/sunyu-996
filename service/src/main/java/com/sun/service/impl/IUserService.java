package com.sun.service.impl;

import com.sun.dto.UserDTO;
import com.sun.mapper.UserMapper;
import com.sun.pojo.User;
import com.sun.service.UserService;
import com.sun.utils.Paying;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IUserService implements UserService<User> {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int getRowsByUserDTO(UserDTO userDTO) {
        return userMapper.getUserListRows(userDTO);
    }

    @Override
    public List<User> getUserListByPaying(Paying<User> paying) {
        return userMapper.getUserListByPaying(paying);
    }
}
